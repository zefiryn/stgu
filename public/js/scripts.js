$('document').ready(function() {

    $('.application:visible').find('.active-field').val(1);
    $('.application').find('.active-field[value="1"]').parents('.application').addClass('visible').show();
    if ($('input[id*="-active"]').length == $('div.application').length) {
        $('#add-new-application').on('click', showNextEntry);
    }
    else {
        $('#add-new-application').hide();
    }

    $('.close-application').on('click', function() {
        hideApplication(this);
    });

    if ($('input[type="hidden"][id*="Cache"]').length > 0) {
        $('input[type="hidden"][id*="Cache"]').each(function(idx, elem){
            if ($(elem).val() != '') {
                displayAttachedFile($(elem));
            }
        });
    }

    $('.file').on('change', function() {
        if ($(this).val() != null) {
            displayAttachedFile($(this));
        }
    });

    $(".project .file-list .jpg a").on('click', function(e){
        e.preventDefault();
    });
    if ($(".project .file-list .jpg a").length > 0) {
        $(".project .file-list .jpg a").fancybox({
            'transitionIn': 'elastic',
            'transitionOut': 'elastic',
            'titlePosition': 'over',
            'speedIn': 600,
            'speedOut': 200,
            'autoScale': true,
            'overlayShow': false
        });
    }
});

function showNextEntry() {
    //show next entry form part
    var application = $('.application').not('.visible').first();
    application.addClass('visible');
    application.show();

    //active part
    application.find('.active-field').val(1);

    if ($('.application').not('.visible').length == 0) {
        $('#add-new-application').attr('disabled', 'disabled');
        $('#add-new-application').off('click', showNextEntry);
    }
}

function hideApplication(button) {
    var application = $(button).parents('.application');
    application.removeClass('visible');
    application.hide();

    application.find('.active-field').val(0);
    if ($('.application').not('.visible').length > 0 && $('#add-new-application').attr('disabled') != undefined) {
        $('#add-new-application').removeAttr('disabled');
        $('#add-new-application').on('click', showNextEntry);
    }
}

function displayAttachedFile(element) {
    element.attr('type') == 'file' ? element.addClass('selected') : element.siblings('input[type="file"]').addClass('selected');
    var filename = element.val().replace("C:\\fakepath\\", '').replace("cache/", '');
    if (filename.length > 30) {
        filename = filename.substr(0, 10) + ' ... ' + filename.slice(-10);
    }
    element.siblings('.add-file').text('ZAŁĄCZONO: ' + filename);
}