function Voting() {
    this.initialize = function (data, url, fetchUrl, statistics, role) {
        this.appData = data ? data : {};
        this.role = role;
        this.currentApp = null;
        this.fetchApp = null;
        this.saveUrl = url;
        this.fetchUrl = fetchUrl;
        this.statistics = statistics;
        this.bindFilters();
        this.bindPopup();
        this.bindVotes();
        this.refreshStats();
    },

    this.bindFilters = function() {
        $('.filters .vote-filter li').on('click', this.filter.bind(this));
        $('.filters .type-filter li').on('click', this.filter.bind(this));
    },

    this.bindPopup = function() {
        $('.voting-applications .item').on('click', this.itemClicked.bind(this));
        $('#app-close').on('click', this.hideEntry.bind(this));
        $('#app-prev').on('click', this.prevEntry.bind(this));
        $('#app-next').on('click', this.nextEntry.bind(this));
    },

    this.bindVotes = function() {
        $('.application-votes > li').on('click', this.saveVote.bind(this));
    },

    this.filter= function(event) {
        var element = $(event.target).closest('li');
        var show = element.hasClass('unselected');
        show ? element.removeClass('unselected') : element.addClass('unselected');
        this.filterItems();
    },

    this.filterItems= function() {
        //show all
        $('.voting-applications .item').show();

        //hide from votes
        var elems = [];
        $('.filters li.unselected').each(function(idx, item) {
            elems.push('li.' + $(item).attr('id'));
        });
        var collection = $('.voting-applications').find(elems.join(', '));
        collection.hide();
        this.refreshStats();
    },

    this.itemClicked = function(event) {
        var item = $(event.target).closest('li.item');
        var applicationId = item.data('application-id');
        this.showEntry(applicationId);
    },

    this.getApplicationData = function(applicationId) {

        var applicationId = parseInt(applicationId);
        if (this.appData[applicationId] == undefined) {
            this.appData[applicationId] = {
                "title":"Art Museum",
                "description":null,
                "category": null,
                "client": null,
                "year": null,
                "projectLink": null,
                "files": {
                    "mono_sign": null,
                    "color_sign": null,
                    "identification_1": null,
                    "identification_2":null,
                    "identification_3":null},
                "vote":"0"
            };
            this.fetchApp = parseInt(applicationId);
            $.ajax(this.fetchUrl, {
                method : 'GET',
                dataType: 'json',
                data: {app_id: applicationId},
                beforeSend: this.fetchAppStart.bind(this),
                success: this.fetchAppSuccess.bind(this),
                complete: this.fetchAppComplete.bind(this)
            });
            return false;
        }
        return this.appData[applicationId];
    },

    this.showEntry = function(applicationId) {
        this.currentApp = applicationId;
        if (this.prepareEntry(applicationId)) {
            $('#entry-overlay').show();
            $('body').addClass('entry-visible', true);
        }
    },

    this.prepareEntry = function(applicationId) {
        var appData = this.getApplicationData(applicationId);
        if (!appData) {
            return false;
        }

        $('.application-votes li').removeClass('selected');
        //basic data
        $('#application-title').text(appData.title);
        var elem = appData.vote !== null ? appData.vote : '';
        $('#app-vote' + elem).addClass('selected');
        $('#current-app-id').text(applicationId);
        $('#application-description').text(appData.description);

        //more info
        $('#app-more-info ul li').remove();
        $('#app-more-info ul').append(this.createNode('li', 'Kategoria: ' + appData.category));
        $('#app-more-info ul').append(this.createNode('li', 'Klient: ' + appData.client));
        $('#app-more-info ul').append(this.createNode('li', 'Rok: ' + appData.year));
        if (this.role == 'admin') {
            $('#app-more-info ul').append(this.createNode('li', '<a href="' + appData.projectLink + '" target="_blank">Więcej</a>'));
        }


        //images
        $('.application-images .pdf-files .mono-sign').empty();
        if (appData.files['mono_sign'] != null) {
            $('.application-images .pdf-files .mono-sign').append(this.createImgNode(appData.files['mono_sign']));
        }

        $('.application-images .pdf-files .color-sign').empty();
        if (appData.files['color_sign'] != null) {
            $('.application-images .pdf-files .color-sign').append(this.createImgNode(appData.files['color_sign']));
        }

        $('.application-images .identifications').empty();
        if (appData.files['identification_1'] != null) {
            $('.application-images .identifications').append(this.createImgNode(appData.files['identification_1'], true));
        }
        if (appData.files['identification_2'] != null) {
            $('.application-images .identifications').append(this.createImgNode(appData.files['identification_2'], true));
        }
        if (appData.files['identification_3'] != null) {
            $('.application-images .identifications').append(this.createImgNode(appData.files['identification_3'], true));
        }

        $('#application-id').text(applicationId);
        return true;
    },

    this.createImgNode = function(url, addLink) {
        var node = document.createElement('img');
        node.setAttribute('src', url);

        if (addLink) {
            var aNode = document.createElement('a');
            aNode.setAttribute('href', url.replace('_big.', '.'));
            aNode.setAttribute('target', '_BLANK');
            aNode.appendChild(node);
            return aNode;
        }
        return node;
    },

    this.createNode = function(node, text) {
        var node = document.createElement(node);
        $(node).html(text);

        return node;
    },

    this.hideEntry = function(event) {
        $('#entry-overlay').hide();
        $('body').removeClass('entry-visible',true);
    },

    this.overlayLoading = function() {
        $('#application-title').text('');
        $('.application-votes li').removeClass('selected');
        var index = $('.voting-applications li.item').index($('#app-'+ this.fetchApp)) + 1;
        $('#current-app-id').text(this.fetchApp);
        $('#application-description').text(null);
        $('#app-more-info ul li').remove();
        $('.application-images .pdf-files .mono-sign').empty();
        $('.application-images .pdf-files .color-sign').empty();
        $('.application-images .identifications').empty();
    }

    this.prevEntry = function(event) {
        var prevElem = $('#app-' + this.currentApp).prev();
        if (prevElem.length == 0) {
            prevElem = $('.voting-applications li.item:last-child');
        }
        appId = prevElem.data('application-id');
        this.showEntry(appId);
    },

    this.nextEntry = function(event) {
        var nextElem = $('#app-' + this.currentApp).next();
        if (nextElem.length == 0) {
            if ($('#next-page').length > 0) {
                $('#next-page')[0].click();
            }
            else {
                nextElem = $('.voting-applications li.item:first-child');
            }

        }
        appId = nextElem.data('application-id');
        this.showEntry(appId);
    },

    this.saveVote = function(event) {
        var element = $(event.target);
        $.ajax(this.saveUrl, {
            method : 'POST',
            dataType: 'json',
            data: {app_id: this.currentApp, value: element.data('value')},
            beforeSend: this.voteStart.bind(this),
            success: this.voteSuccess.bind(this),
            complete: this.voteComplete.bind(this)
        });
    },
    this.voteStart = function() {

    },
    this.voteSuccess = function(transport) {
        var appData = this.getApplicationData([transport.app_id])
        var prevVote = appData.vote;
        this.appData[transport.app_id].vote = transport.value;
        this.showEntry(transport.app_id);
        this.updateStatistics(prevVote, transport.value);
        this.updateItem(transport.app_id);
    },

    this.voteComplete = function(transport) {

    },

    this.fetchAppStart = function() {
        if ($('body').hasClass('entry-visible')) {
            this.overlayLoading();
        }
        else {
            $('#app-' + this.fetchApp).addClass('loading');
        }
    },
    this.fetchAppSuccess = function(transport) {
        if (transport.success == true) {
            this.appData[parseInt(transport.app_id)] = transport.data;
            this.showEntry(parseInt(transport.app_id));
        }
    },

    this.fetchAppComplete = function(transport) {
        $('#app-' + this.fetchApp).removeClass('loading');
    },

    this.updateStatistics = function(oldVote, newVote) {
        if (oldVote === null) {
            this.statistics[0]--;
        }
        else if (oldVote == 0) {
            this.statistics[2]--;
        }
        else if (oldVote == 1) {
            this.statistics[1]--;
        }

        if (newVote === null) {
            this.statistics[0]++;
        }
        else if (newVote == 0) {
            this.statistics[2]++;
        }
        else if (newVote == 1) {
            this.statistics[1]++;
        }
        this.refreshStatistics();
    },

    this.refreshStatistics= function() {
        $('#stats')[0].childNodes[1].nodeValue = '/' + this.statistics[0];
        $('#stats1')[0].childNodes[1].nodeValue = '/' + this.statistics[1];
        $('#stats0')[0].childNodes[1].nodeValue = '/' + this.statistics[2];
    },

    this.updateItem = function(appId) {
        var span = $('#app-' + appId + ' .vote_status');
        span.removeClass();
        span.addClass('vote_status');
        span.addClass('vote-val' + this.appData[appId].vote);
        $('#app-' + appId).removeClass('vote-val vote-val1 vote-val0').addClass('vote-val' + this.appData[appId].vote);

        this.filterItems();
    },

    this.refreshStats = function() {
        $('.type-filter li').each(function() {
            var node = $(this).find('.stats span');
            node.text($('li.item.' + $(this).attr('id') + ':visible').length);
        });

        $('.vote-filter li').each(function() {
            var node = $(this).find('span > span');
            node.text($('li.item span.' + $(this).attr('id') + ':visible').length);
        });
    }
}