<?php

class STGU_Controller_Action extends Zefir_Controller_Action
{
	public function init() {
		$this->_helper->Redirector->setUseAbsoluteUri(true);
		parent::init();
	}
}