<?php
/**
 * @package Application_Form_Application
 */

/**
 * Declaration of the application form
 *
 * @author zefiryn
 * @since  Feb 2011
 */
class Application_Form_Application extends Zefir_Form {

    protected $_type;
    protected $_user;
    protected $_applications;

    public function __construct($type, $applications) {
        $this->_type = $type;
        $this->_applications = $applications;
        parent::__construct();

    }

    public function setUser($user) {
        $this->_user = $user;
    }

    public function getUser() {
        return $this->_user;
    }

    public function getApplications() {
        return $this->_applications;
    }

    public function init() {
        $L = $this->_regex['L'];
        $N = $this->_regex['N'];
        $S = $this->_regex['S'];
        parent::init();

        $this->setMethod('post');
        $this->setName('PageForm');
        $this->setTranslator(Zend_Registry::get('Zend_Translate'));
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        $this->addElementPrefixPath('STGU_Decorator', 'STGU/Form/Decorator', 'decorator');
        $this->addPrefixPath('STGU_Decorator', 'STGU/Form/Decorator', 'decorator');
        $this->addElementPrefixPath('Zefir_Filter', 'Zefir/Filter/', 'filter');


        $element = $this->createElement('hidden', 'edition_id');
        $element->setDecorators(array('ViewHelper'));
        $this->addElement($element);

        $element = $this->createElement('hidden', 'application_id');
        $element->setDecorators(array('ViewHelper'));
        $this->addElement($element);

        if($this->_type != 'new') {
            $element = $this->createElement('hidden', 'user_id');
            $element->setDecorators(array('ViewHelper'));
            $this->addElement($element);
        }

        if($this->_type == 'new') {
            $userSubForm = new Application_Form_User('subform');
            $userSubForm->setDecorators(array('FormElements'));
            $userSubForm->setIsArray(true);
            $userSubForm->removeElement('csrf');
            $userSubForm->removeElement('role');
            $this->addSubForm($userSubForm, 'user');
        }

        $element = $this->createElement('checkbox', 'copyright');
        $element->setAttribs(array('class' => 'checkbox'))
            ->setLabel('copyright_checkbox', array('tag' => 'label'))
            ->setDecorators(array(
                array('ViewHelper'),
                array('MyLabel', array('placement' => 'append')),
                array('ErrorMsg', array('image' => false)),
            ))
            ->setRequired(true)
            ->addValidators(array(
                new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::ZERO)
            ));
        $this->addElement($element);

        $element = $this->createElement('checkbox', 'rules_agreement');
        $element->setAttribs(array('class' => 'checkbox'))
            ->setLabel('rules_acknowledgement', array('tag' => 'label'))
            ->setDecorators(array(
                array('ViewHelper'),
                array('MyLabel', array('placement' => 'append')),
                array('ErrorMsg', array('image' => false)),
            ))
            ->setRequired(true)
            ->addValidators(array(
                new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::ZERO)
            ));
        $this->addElement($element);

        $element = $this->createElement('checkbox', 'newsletter');
        $element->setAttribs(array('class' => 'checkbox'))
            ->setLabel('newsletter_label', array('tag' => 'label'))
            ->setDecorators(array(
                array('ViewHelper'),
                array('MyLabel', array('placement' => 'append')),
                array('ErrorMsg', array('image' => false)),
            ))
            ->setRequired(false);
        $this->addElement($element);

        for ($i = 1; $i <= $this->_applications; $i++) {
            $entrySubForm = new Application_Form_Application_Entry($this->_type, $i);
            $entrySubForm->removeElement('csrf');
            $this->addSubForm($entrySubForm, "entry[{$i}]");
        }

        /**
         * SUBMIT
         */
        $this->_createStandardSubmit('application_submit');
        $this->addDisplayGroup(array('leave', 'submit'), 'submitFields')
            ->setDisplayGroupDecorators(array(
                'FormElements',
                array('Fieldset', array('class' => 'submit'))
            ));
    }


}

