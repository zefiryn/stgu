<?php

/**
 * @package Application_Form_Login
 */

/**
 * Declaration of the login form
 *
 * @author zefiryn
 * @since  Feb 2011
 */
class Application_Form_Restore extends Zefir_Form {

    /**
     * Inital; set fields, decorators and validators
     *
     * @access public
     * @return void
     */
    public function init() {
        parent::init();

        $this->setMethod('post');
        $this->setName('PageForm');

        $login = $this->createElement('text', 'email');
        $login->setAttribs(array('size' => 55, 'class' => 'width1 normal'))
            ->setLabel('login_email')
            ->setRequired(true)
            ->addValidator(new Zefir_Validate_Login(3, 50))
            ->setDecorators($this->_getZefirDecorators());
        $this->addElement($login);

        $this->addElement('hash', 'csrf', array(
            'ignore' => true,
            'decorators' => array(array('ViewHelper'),
                array('Errors'))
        ));

        $this->_createStandardSubmit('login_submit');
    }

}