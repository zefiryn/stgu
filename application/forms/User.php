<?php
/**
 * @package Application_Form_User
 */
/**
 * Declaration of the user form
 * @author zefiryn
 * @since Mar 2011
 */

class Application_Form_User extends Zefir_Form
{
	protected $_type;
    protected $_role;

	public function __construct($type = 'form', $options = null, $role = 'user')
	{
		$this->_type = $type;
        $this->_role = $role;
		parent::__construct($options);
	}

	public function init()
	{
		$L = $this->_regex['L'];
		$N = $this->_regex['N'];
		$S = $this->_regex['S'];
		$E = $this->_regex['E'];
		$B = $this->_regex['B'];
		parent::init();

		$this->setMethod('post');
		$this->setName('UserForm');
		$this->setTranslator(Zend_Registry::get('Zend_Translate'));

		$this->addElementPrefixPath('GP_Decorator', 'STGU/Form/Decorator', 'decorator');
		$this->addPrefixPath('GP_Decorator', 'STGU/Form/Decorator', 'decorator');

		$element = $this->createElement('hidden', 'user_id')->setDecorators(array('ViewHelper'));
		$this->addElement($element);

		$element = $this->createElement('password', 'password');
		$element->setAttribs(array('class' => 'width1'))
			->setLabel('password')
            ->setRequired(($this->_type == 'new' || $this->_type == 'subform'))
			->setDecorators($this->_getStandardDecorators());
		$this->addElement($element);

		$element = $this->createElement('text', 'name');
		$element->setAttribs(array('class' => 'width1'))
			->setLabel('user_name')
			->setDecorators($this->_getZefirDecorators())
            ->setRequired($this->_type == 'subform')
			->addFilters(array(
				new Zend_Filter_StringTrim()
			))
			->addValidators(array(
				new Zend_Validate_Regex('/^['.$L.'\- ]+$/'),
				new Zend_Validate_StringLength(array('min' => 3, 'max' => 150))
			));
		$this->addElement($element);

        $element = $this->createElement('text', 'company');
        $element->setAttribs(array('class' => 'width1'))
            ->setLabel('company')
            ->setRequired(false)
            ->setDecorators($this->_getZefirDecorators())
            ->addFilters(array(
                new Zend_Filter_StringTrim()
            ))
            ->addValidators(array(
                new Zend_Validate_Regex('/^['.$L.$N.$S.'\- ]+$/'),
                new Zend_Validate_StringLength(array('min' => 3, 'max' => 150))
            ));
        $this->addElement($element);

		$element = $this->createElement('text', 'address');
		$element->setAttribs(array('class' => 'width1'))
			->setLabel('address')
            ->setRequired($this->_role != 'admin')
			->setDecorators($this->_getZefirDecorators())
			->addValidators(array(
				new Zend_Validate_Regex('/^['.$L.$N.$S.' ]+$/'),
				new Zend_Validate_StringLength(array('min' => 3, 'max' => 200))
			));
		$this->addElement($element);

		$element = $this->createElement('text', 'phone');
		$element->setAttribs(array('class' => 'width1'))
			->setLabel('phone')
			->setDecorators($this->_getZefirDecorators())
            ->setRequired($this->_role != 'admin')
			->addValidators(array(
				new Zend_Validate_Regex('/^(\+[0-9]{2,3}( )?)?[0-9 ]{4,12}$/'),
				new Zend_Validate_StringLength(array('min' => 5, 'max' => 15))
			));
		$this->addElement($element);

		$element = $this->createElement('text', 'email');
		$element->setAttribs(array('class' => 'width1'))
			->setLabel('email')
			->setDecorators($this->_getZefirDecorators())
			->setRequired(TRUE)
			->addValidators(array(
				new Zend_Validate_EmailAddress(),
				new Zend_Validate_StringLength(array('min' => 3, 'max' => 35))
			));
		$this->addElement($element);

        if ($this->_type == 'form') {
            $this->_createStandardSubmit('user_submit');
            $this->addDisplayGroup(array('leave', 'submit'), 'submitFields')
                ->setDisplayGroupDecorators(array(
                    'FormElements',
                    array('Fieldset', array('class' => 'submit'))
                ));
        }
	}
}
