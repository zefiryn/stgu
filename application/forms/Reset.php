<?php

/**
 * @package Application_Form_Login
 */

/**
 * Declaration of the login form
 *
 * @author zefiryn
 * @since  Feb 2011
 */
class Application_Form_Reset extends Zefir_Form {

    /**
     * Inital; set fields, decorators and validators
     *
     * @access public
     * @return void
     */
    public function init() {
        parent::init();

        $this->setMethod('post');
        $this->setName('PageForm');

        $password = $this->createElement('password', 'password');
        $password->setAttribs(array('size' => 100, 'class' => 'width1 normal', 'maxlength' => 100))
            ->setLabel('new_password')
            ->setRequired(true)
            ->setDecorators(array(
                array('PasswordField'),
                array('MyLabel', array('placement' => 'prepend', 'tag' => 'label')),
                array('ErrorMsg', array('image' => FALSE)),
            ));
        $this->addElement($password);

        $this->addElement('hash', 'csrf', array(
            'ignore' => true,
            'decorators' => array(array('ViewHelper'),
                array('Errors'))
        ));

        $this->_createStandardSubmit('login_submit');
    }

}