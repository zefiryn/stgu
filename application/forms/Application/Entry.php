<?php
/**
 * @package Application_Form_Application
 */

/**
 * Declaration of the application form
 *
 * @author zefiryn
 * @since  APr 2015
 */
class Application_Form_Application_Entry extends Zefir_Form {

    protected $_type;
    protected $_number;

    public function __construct($type, $number) {
        $this->_type = $type;
        $this->_number = $number;
        parent::__construct();
    }

    public function getNumber() {
        return $this->_number;
    }

    public function init() {
        $L = $this->_regex['L'];
        $N = $this->_regex['N'];
        $S = $this->_regex['S'];
        parent::init();

        $this->setName("entry");
        $this->setTranslator(Zend_Registry::get('Zend_Translate'));
        $this->setIsArray(true);
        $this->setEnctype(Zend_Form::ENCTYPE_MULTIPART);

        $this->setDecorators(array(
            array('viewScript', array('viewScript' => 'forms/_entryForm.phtml'))
        ));

        $this->addElementPrefixPath('STGU_Decorator', 'STGU/Form/Decorator', 'decorator');
        $this->addPrefixPath('STGU_Decorator', 'STGU/Form/Decorator', 'decorator');
        $this->addElementPrefixPath('Zefir_Filter', 'Zefir/Filter/', 'filter');

        $element = $this->createElement('hidden', 'active');
        $element->setAttribs(array('class' => 'active-field'))
                ->setDecorators(array('ViewHelper'))
                ->setValue($this->_number == 1 ? 1 : 0);
        $this->addElement($element);

        $element = $this->createElement('text', 'work_subject');
        $element->setAttribs(array('class' => 'width1'))
            ->setLabel('work_subject')
            ->setDecorators($this->_getZefirDecorators())
            ->setRequired(true)
            ->addFilters(array(
                new Zend_Filter_StringTrim()
            ))
            ->addValidators(array(
                new Zend_Validate_Regex('/^[' . $L . $N . $S . '\ ]*$/'),
                new Zend_Validate_StringLength(array('min' => 3, 'max' => 300))
            ));
        $this->addElement($element);

        $element = $this->createElement('text', 'authors');
        $element->setAttribs(array('class' => 'width1'))
            ->setLabel('studio_name')
            ->setDecorators($this->_getZefirDecorators())
            ->setRequired(false)
            ->addFilters(array(
                new Zend_Filter_StringTrim()
            ))
            ->addValidators(array(
                new Zend_Validate_Regex('/^[' . $L . $N . $S . '\ ]*$/'),
                new Zend_Validate_StringLength(array('max' => 255))
            ));
        $this->addElement($element);

        $element = $this->createElement('text', 'customer');
        $element->setAttribs(array('class' => 'width1'))
            ->setLabel('customer')
            ->setDecorators($this->_getZefirDecorators())
            ->setRequired(false)
            ->addFilters(array(
                new Zend_Filter_StringTrim()
            ))
            ->addValidators(array(
                new Zend_Validate_Regex('/^[' . $L . $N . $S . '\ ]*$/'),
                new Zend_Validate_StringLength(array('min' => 3, 'max' => 800))
            ));
        $this->addElement($element);

        $element = $this->createElement('text', 'project_year');
        $element->setAttribs(array('class' => 'width1'))
            ->setLabel('project_year')
            ->setDecorators($this->_getZefirDecorators())
            ->setRequired(true)
            ->addValidators(array(
                new Zend_Validate_Between(array('min' => 2000, 'max' => 2015, 'inclusive' => true))
            ));
        $this->addElement($element);


        $namePrefix = $this->_number > 0 ? "entry[{$this->_number}]" : "entry";
        $subForm = new Application_Form_Application_File('mono_sign', $this->_number, $this->_type, '300×300mm, PDF, CMYK', 'pdf');
        $this->addSubForm($subForm, "{$namePrefix}[mono_sign]");

        $subForm = new Application_Form_Application_File('color_sign', $this->_number, $this->_type, '300×300mm, PDF, CMYK', 'pdf');
        $this->addSubForm($subForm, "{$namePrefix}[color_sign]");


        $subForm = new Application_Form_Application_File('identification_1', $this->_number, $this->_type, '1754×1240px, JPG, RGB', 'jpg');
        $this->addSubForm($subForm, "{$namePrefix}[identification_1]");

        $subForm = new Application_Form_Application_File('identification_2', $this->_number, $this->_type, '1754×1240px, JPG, RGB', 'jpg', false);
        $this->addSubForm($subForm, "{$namePrefix}[identification_2]");

        $subForm = new Application_Form_Application_File('identification_3', $this->_number, $this->_type, '1754×1240px, JPG, RGB', 'jpg', false);
        $this->addSubForm($subForm, "{$namePrefix}[identification_3]");

        $work_type = new Application_Model_WorkTypes();
        $element = $this->createElement('select', 'work_type_id');
        $element->setAttribs(array('class' => 'width1'))
            ->setLabel('work_type')
            ->setMultiOptions($work_type->getWorkTypes())
            ->setDecorators($this->_getStandardDecorators())
            ->setRequired(TRUE)
            ->addValidators(array(
                new Zend_Validate_NotEmpty(Zend_Validate_NotEmpty::ZERO),
                new Zend_Validate_Digits()
            ));
        $this->addElement($element);

        $element = $this->createElement('textarea', 'work_desc');
        $element->setAttribs(array('class' => 'desc'))
            ->setLabel('work_desc')
            ->setDecorators($this->_getZefirDecorators())
            ->setRequired(true)
            ->addFilters(array(
                new Zend_Filter_StringTrim()
            ))
            ->addValidators(array(
                new Zend_Validate_NotEmpty(array('type' => 'string'))
            ));
        $this->addElement($element);

        $this->_createStandardSubmit('save_application');
    }
}
