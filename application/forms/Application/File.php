<?php
/**
 * @package Application_Form_File
 */

/**
 * Declaration of the application file form
 *
 * @author zefiryn
 * @since  Feb 2011
 */
class Application_Form_Application_File extends Zefir_Form_SubForm {

    protected $_label;
    protected $_type;
    protected $_description;
    protected $_fileType;
    protected $_showLabel;
    protected $_number;

    public function __construct($label, $number, $type, $description, $fileType, $showLabel = true) {
        $this->_label = $label;
        $this->_description = $description;
        $this->_type = $type;
        $this->_fileType= $fileType;
        $this->_showLabel = $showLabel;
        $this->_number = $number;
        parent::__construct();

    }

    public function getLabel() {
        return $this->_label;
    }

    public function init() {
        parent::init();

        $this->setName('FileForm');
        $this->setTranslator(Zend_Registry::get('Zend_Translate'));
        $this->setIsArray(true);

        $this->setDecorators(array(
            array('viewScript', array('viewScript' => 'forms/_fileForm.phtml'))
        ));

        $this->addElementPrefixPath('STGU_Decorator', 'STGU/Form/Decorator', 'decorator');
        $this->addPrefixPath('STGU_Decorator', 'STGU/Form/Decorator', 'decorator');

        $appSettings = Zend_Registry::get('appSettings');
        $options = Zend_Registry::get('options');

        $element = $this->createElement('hidden', 'application_id', array(
            'decorators' => array('ViewHelper')
        ));
        $this->addElement($element);

        $element = $this->createElement('hidden', 'file_id', array(
            'decorators' => array('ViewHelper')
        ));
        $this->addElement($element);

        $element = $this->createElement('hidden', $this->_label.'_'.$this->_number . 'Cache', array(
            'decorators' => array('ViewHelper')
        ));
        $this->addElement($element);

        $element = new Zend_Form_Element_File($this->_label.'_'.$this->_number);
        $element->setDestination(APPLICATION_PATH . '/../public' . $options['upload']['cache'])
            ->setAttribs(array('class' => 'file'))
            ->setRequired(false)
            ->setLabel($this->_label)
            ->setDescription($this->_description)
            ->setAllowEmpty(true)
            ->addValidators(array(
                $this->_getExtensionValidator(),
                array('Size', false, array('max' => $appSettings->max_file_size)),
            ))
            ->setDecorators($this->_getDecorators());
        $this->addElement($element);
    }


    protected function _getExtensionValidator() {
        if ($this->_fileType == 'jpg') {
            $extValidator = array('Extension', true, array(false, 'jpg,jpeg'));
        }
        elseif ($this->_fileType == 'pdf') {
            $extValidator = array('Extension', true, array(false, 'pdf'));
        }

        return $extValidator;
    }

    protected function _getDecorators() {
        $decorators = array(
            array('File'),
            array('ErrorMsg'),
            array('UnderDescription', array('class' => 'add-file', 'placement' => 'append', 'tag' => 'span'))
        );

        if ($this->_showLabel) {
            $decorators[] = array('MyLabel', array('placement' => 'prepend', 'tag' => 'label'));
        }
        return $decorators;
    }

    public function getNumber() {
        return $this->_number;
    }
}