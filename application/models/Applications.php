<?php

class Application_Model_Applications extends STGU_Application_Model {
    public $application_id;
    public $edition_id;
    public $user_id;
    public $work_subject;
    public $authors;
    public $work_type_id;
    public $work_desc;
    public $customer;
    public $project_year;
    public $application_date;
    public $active;
    public $rules_agreement;
    public $copyright;
    protected $edition;
    protected $user;
    protected $work_type;
    protected $files;
    protected $votes;

    protected $_update = false;

    protected $_dbTableModelName = 'Application_Model_DbTable_Applications';

    public function __construct($id = null, array $options = null) {
        return parent::__construct($id, $options);
    }

    public function populateFromForm($data) {
        $appSettings = Zend_Registry::get('appSettings');
        parent::populateFromForm($data);

        if($this->application_date == null && $this->application_id == null) {
            $this->application_date = time();
        }

        if($this->active == null) {
            $this->active = 1;
        }

        if(isset($data['user_id'])) {
            $this->user = new Application_Model_Users($data['user_id']);
        }

        if($this->edition_id == null) {
            $this->edition_id = $appSettings->current_edition;
        }

        $fileForms = array('mono_sign', 'color_sign', 'identification_1', 'identification_2', 'identification_3');
        $this->files = array();
        foreach($fileForms as $field) {
            if(array_key_exists($field, $data) && array_key_exists($field . 'Cache', $data[$field])) {
                $file = new Application_Model_Files();
                $file->file_id = $data[$field]['file_id'];
                $file->path = $data[$field][$field . 'Cache'];
                $file->type = $field;
                $file->save = $data[$field]['save'];
                $this->files[] = $file;
            }
        }
    }

    public function delete() {
        return $this->getDbTable()->delete($this);
    }

    public function prepareFormArray() {
        $data = array(
            'application_id' => $this->application_id,
            'edition_id' => $this->edition_id,
            'user_id' => $this->user_id,
            'work_subject' => $this->work_subject,
            'work_type_id' => $this->work_type_id,
            'work_desc' => $this->work_desc,
            'authors' => $this->authors,
            'customer' => $this->customer,
            'project_year' => $this->project_year,
            'rules_agreement' => true,
            'copyright' => true,
        );
        foreach($this->__get('files') as $file) {
            /** @var Application_Model_Files $file */
            $data[$file->type][$file->type.'_0Cache'] = $file->path;
            $data[$file->type]['application_id'] = $this->application_id;
            $data[$file->type]['file_id'] = $file->file_id;
        }

        return $data;
    }

    public function getApplications($sort = null, $size = null, $page = null) {
        $sort = strstr($sort, 'work_type_id') ? 'a.' . $sort : $sort;

        $sort = $sort != null ? array($sort, 'name ASC', 'application_date ASC') : array('application_date ASC', 'name ASC');

        $user = null;
        if (!in_array(Zend_Registry::get('role'), array('admin', 'juror'))) {
            $user = $this->_getUser();
        }
        $rowset = $this->getDbTable()->getAllApplications($sort, $user, $size, $page);

        $applications = array();
        foreach($rowset as $row) {
            $application_id = $row[$this->getDbTable()->getPrimaryKey()];
            if (!array_key_exists($application_id, $applications)) {
                /** @var Application_Model_Applications $application */
                $application = new $this;
                $application->populate($row);
                $vote = new Application_Model_Votes();
                $vote->populate($row);
                $application->setVote($vote);
                $applications[$application_id] = $application;
            }

            $file = new Application_Model_Files();
            $file->populate($row);
            /** @var Application_Model_Applications  $app */
            $app = $applications[$application_id];
            $app->addFile($file);

        }
        return $applications;
    }

    public function populate($row) {
        parent::populate($row);

        if (array_key_exists('email', $row)) {
            $user = new Application_Model_Users();
            $user->populate($row);
            $this->user = $user;
        }

        if (array_key_exists('work_type_name', $row)) {
            $workType = new Application_Model_WorkTypes();
            $workType->populate($row);
            $this->work_type = $workType;
        }
        return $this;
    }

    public function setUser(Application_Model_Users $user) {
        $this->user = $user;
    }

    public function getUser() {
        return $this->user;
    }

    public function getFiles() {
        return $this->files;
    }

    public function getFile($type) {
        if ($this->files == null) {
            $this->__get('files');
        }
        foreach ($this->files as $file) {
            /** @var Application_Model_Files $file */
            if ($file->type == $type) {
                return $file;
            }
        }

        return false;
    }

    public function getMonoImage() {
        try {
            $image = $this->getFile('mono_sign');
            return $image ? $image->getImageUrl('thumb', false) : null;
        }
        catch (Exception $e) {
            $this->_log($e->__toString());
            return $this->getFile('identification_1') ? $this->getFile('identification_1')->getImage('thumb') : null;
        }

    }

    public function getColorImage() {
        try {
            $image = $this->getFile('color_sign');
            return $image ? $image->getImageUrl('thumb', false) : $this->getMonoImage();
        }
        catch (Exception $e) {
            $this->_log($e->__toString());
            return $this->getFile('identification_1') ? $this->getFile('identification_1')->getImage('thumb') : null;
        }

    }

    public function addFile(Application_Model_Files $file) {
        if ($this->files == null) {
            $this->files = array();
        }

        $this->files[$file->file_id] = $file;
    }

    public function setVote(Application_Model_Votes $vote) {
        $this->votes = $vote;
    }

    public function getVote() {
        if (null == $this->votes) {
            $this->__get('votes');
            $this->votes = is_array($this->votes) && !empty($this->votes) ? array_shift($this->votes) : new Application_Model_Votes();
        }

        return $this->votes;
    }

    public function getFiltersStats() {
        /** @var Zend_Db_Table_Rowset $rowset */
        $rowset = $this->getDbTable()->getFilterStats();
        $stats = array();
        foreach($rowset->toArray() as $row) {
            $stats['type-' . $row['work_type_id']] = $row['size'];
        }

        return $stats;
    }

    public function getVoteStats() {
        /** @var Zend_Db_Table_Rowset $rowset */
        $rowset = $this->getDbTable()->getVoteStats();
        $stats = array();
        foreach($rowset->toArray() as $row) {
            $idx = 0;
            if ($row['value'] !== null) {
                $idx = $row['value'] == '1' ? 1 : 2;
            }
            $stats[$idx] =$row['size'];
        }

        return $stats;
    }

    public function getMaxPage($size) {
        $count = $this->getDbTable()->countRows();
        return ceil($count / $size);
    }
}

