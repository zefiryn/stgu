<?php

class Application_Model_Votes extends STGU_Application_Model {
    public $vote_id;
    public $application_id;
    public $value;
    protected $application;

    protected $_dbTableModelName = 'Application_Model_DbTable_Votes';

    public function __construct($id = null, array $options = null) {
        return parent::__construct($id, $options);
    }

    public function findByApplication($application) {
        $id = is_object($application)? $application->application_id : $application;
        return $this->find(array('application_id' => $id));
    }

}

