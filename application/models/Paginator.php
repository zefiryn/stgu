<?php
class Application_Model_Paginator extends Zend_Paginator_Adapter_DbSelect
{
	/**
	 * Returns an array of items for a page.
	 *
	 * @param  integer $offset Page offset
	 * @param  integer $itemCountPerPage Number of items per page
	 * @return array
	 */
	public function getItems($offset, $itemCountPerPage)
	{
		$rows = parent::getItems($offset, $itemCountPerPage);

		$tasks = array();
		foreach ($rows as $row) {
			$task = new Application_Model_Diplomas();
			$tasks[] = $task->populate($row);
		}
		return $tasks;
	}
}