<?php

class Application_Model_Files extends STGU_Application_Model {
    public $file_id;
    public $application_id;
    public $path;
    public $type;
    public $save = true;
    protected $application;
    protected $_image = array(
        'property' => 'path',
        'dir' => '/assets/applications'
    );
    protected $_imageData = array(
        'thumb' => array(
            'width' => 200,
            'height' => 200,
            'crop' => false,
            'ratio' => 'width' //save ratio according to new width
        ),
        'small' => array(
            'width' => 450,
            'height' => 450,
            'crop' => false,
            'ratio' => 'width'    //save ratio according to new width
        ),
        'miniature' => array(
            'width' => 80,
            'height' => 80,
            'crop' => false,
            'ratio' => 'both'    //save ratio according to bigger dimension
        ),
        'big' => array(
            'width' => 900,
            'height' => 300,
            'crop' => false,
            'ratio' => 'both'    //save ratio according to bigger dimension
        ),
    );

    protected $_dbTableModelName = 'Application_Model_DbTable_Files';


    public function __construct($id = null, array $options = null) {
        return parent::__construct($id, $options);
    }

    public function resize($orientation, $size) {
        $path = APPLICATION_PATH . '/../public/assets/applications/' . $this->_path;

        if(is_file($path)) {
            $file_size = getimagesize($path);
            $width = $file_size[0];
            $height = $file_size[1];

            if($orientation == 'width') {
                $ratio = $size / $width;
                $width = $size;
                $height = $ratio * $height;
            }
            else {
                $ratio = $size / $height;
                $height = $size;
                $width = $ratio * $width;
            }
        }

        return 'width="' . $width . '" height="' . $height . '"';
    }

    public function getFileFolder() {
        return '/' . substr($this->path, 0, strrpos($this->path, '/'));
    }

    public function getFileName() {
        return basename($this->path);
    }

    public function getFileType() {
        return substr($this->getFileName(), strrpos($this->getFileName(),'.')+1);
    }

    public function getPath() {
        $options = Zend_Registry::get('options');
        return $options['upload']['applications'].'/'.$this->path;
    }

    public function getAbsolutePath() {
        return APPLICATION_PATH . '/../public' .$this->getPath();
    }

    public function delete() {
        parent::delete();

        return $this;
    }

    public function getImageUrl($type = 'thumb', $convert = true) {
        if ($this->getFileType() == 'pdf') {
            if (!file_exists($this->getAbsolutePath().'.png') && $convert) {
                try {
                    $this->convertToImage();
                }
                catch (Exception $e) {
                    return null;
                }
            }
            $this->path = $this->path.'.png';
        }
        return $this->getImage($type);
    }

    public function convertToImage() {
        $im = new Imagick();
        $im->setResolution(200,200);
        $im->readimage($this->getAbsolutePath());
        $im->setImageFormat('png');
        $im->transformImageColorspace(Imagick::COLORSPACE_SRGB);
        $im->writeImage($this->getAbsolutePath().'.png');
        $im->clear();
        $im->destroy();
        unset($im);
    }

}

