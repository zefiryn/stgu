<?php

class Application_Model_DbTable_Users extends Zefir_Application_Model_DbTable {

    protected $_raw_name = 'users';
    protected $_name = '';
    protected $_primary = 'user_id';

    /**
     * An array of parent table information
     *
     * @var array
     */
    protected $_hasMany = array(
        'applications' => array(
            'model' => 'Application_Model_Applications',
            'refColumn' => 'user_id',
        )
    );

    /**
     *
     * Get user role
     *
     * @param string $email
     * @return string
     */
    public function getUserRole($email) {
        $select = $this->select()->where('email = ?', $email);
        $row = $this->fetchRow($select);

        return ($row->role);
    }

    /**
     * Save or update user data in the database
     *
     * @param Application_Model_Users $user
     * @throws Zend_Exception
     * @return Application_Model_Users $user
     */
    public function save(Application_Model_Users $user) {

        $id = $user->user_id;

        if($id != null) {
            $row = $this->find($id)->current();
        }

        else {
            if($id != null) {
                throw new Zend_Exception('Incorrect user');
            }
            else {
                $row = $this->createRow();
            }
        }

        $row->name = $user->name;
        $row->company = $user->company;
        $row->address = $user->address;
        $row->phone = $user->phone;
        $row->email = $user->email;
        $row->token = $user->token;
        $row->token_time = $user->token_time;
        if($user->_role != null) {
            $row->role = $user->_role;
        }
        if($user->_password != null) {
            $salt = $this->_generateSalt();
            $row->password = sha1($user->_password . $salt);
            $row->salt = $salt;
        }

        if($row->save()) {
            if(!$id) {
                $user->user_id = $id = $this->getAdapter()->lastInsertId();
            }
        }
        else {
            throw new Zend_Exception('Couldn\'t save data');
        }
    }

    /**
     *
     * @param int                     $id
     * @return null|Zend_Db_Table_Row_Abstract
     */
    public function findUser($id) {
        if(ctype_digit($id)) {
            $row = $this->find($id)->current();
        }
        else {
            $row = $this->fetchRow($this->select()->where('email = ?', $id));
        }

        return $row;

    }

    protected function _generateSalt($max = 15) {
        $characterList = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%&*?";
        $i = 0;
        $salt = "";
        while($i < $max) {
            $salt .= $characterList{mt_rand(0, (strlen($characterList) - 1))};
            $i++;
        }

        return $salt;
    }

}

