<?php

class Application_Model_DbTable_Applications extends Zefir_Application_Model_DbTable {

    /**
     * Name of the table without prefix
     *
     * @var string
     */
    protected $_raw_name = 'applications';

    /**
     * Primary key of the table
     *
     * @var string
     */
    protected $_primary = 'application_id';


    protected $_belongsTo = array(

        'edition' => array(
            'model' => 'Application_Model_Editions',
            'column' => 'edition_id',
            'refColumn' => 'edition_id'
        ),

        'user' => array(
            'model' => 'Application_Model_Users',
            'column' => 'user_id',
            'refColumn' => 'user_id'
        ),

        'work_type' => array(
            'model' => 'Application_Model_WorkTypes',
            'column' => 'work_type_id',
            'refColumn' => 'work_type_id'
        ),

    );


    /**
     * An array of parent table information
     *
     * @var array
     */
    protected $_hasMany = array(
        'files' => array(
            'model' => 'Application_Model_Files',
            'refColumn' => 'application_id',
        ),
        'votes' => array(
            'model' => 'Application_Model_Votes',
            'refColumn' => 'application_id',
        )
    );

    /**
     * constructor
     *
     * @access public
     * @param array $config
     */
    public function __construct($config = array()) {
        return parent::__construct($config);
    }

    public function getAllApplications($sort, $currentUser = null, $size = null, $page = null) {
        $user = new Application_Model_Users();
        $user_table = $user->getDbTable()->getTableName();

        $work_type = new Application_Model_WorkTypes();
        $work_type_table = $work_type->getDbTable()->getTableName();

        $files = new Application_Model_Files();
        $files_table = $files->getDbTable()->getTableName();

        $votes = new Application_Model_Votes();
        $votes_table = $votes->getDbTable()->getTableName();

        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('a' => $this->_name))
            ->join(array('u' => $user_table), 'a.user_id = u.user_id')
            ->join(array('w' => $work_type_table), 'a.work_type_id= w.work_type_id')
            ->joinLeft(array('f' => $files_table), 'a.application_id= f.application_id', array('file_id', 'path', 'type'))
            ->joinLeft(array('v' => $votes_table), 'a.application_id= v.application_id', array('vote_id', 'value'))
            ->order($sort);

        if ($currentUser != null) {
            //get only specified user applications
            $select->where('a.user_id = ?', $currentUser->user_id);
        }

        if ($size) {
            /** @var Zend_Db_Table_Select $ids */
            $ids = $this->select()
                ->setIntegrityCheck(false)
                ->from(array('a' => $this->_name), 'application_id')
                ->join(array('u' => $user_table), 'a.user_id = u.user_id', null)
                ->join(array('w' => $work_type_table), 'a.work_type_id= w.work_type_id', null)
                ->order($sort);
            $page = $page == null ? 0 : $page - 1;
            $ids->limit($size, $page * $size);

            /** @var Zend_Db_Table_Rowset $rowset */
            $rowset =$this->fetchAll($ids);
            $select->where('a.application_id IN (?)', array_values($rowset->toArray()));
        }

        return $this->fetchAll($select);
    }

    /**
     * Save or update application data in the database
     *
     * @param Application_Model_Applications $application
     * @throws Zend_Exception
     * @return Application_Model_Applications $application
     */
    public function save(Application_Model_Applications $application) {
        if($application->application_id != null) {
            $oldData = new Application_Model_Applications($application->application_id);
        }
        else {
            $oldData = null;
        }

        //check if there is user added
        if($application->user_id == null && $application->getUser()) {
            $application->user_id = $application->getUser()->user_id;
        }

        //save application data
        try {
            $application = parent::save($application);
            if (!$application->getUser()) {
                $application->setUser(new Application_Model_Users($application->user_id));
            }

        }
        catch (Zend_Exception $e) {
            throw $e;
        }

        //create user directory
        $userDir = $this->_getUserDir($application, $oldData);

        //copy uploaded files
        $this->_saveUserFiles($application, $userDir, $oldData);

        return $application;

    }

    public function delete(Application_Model_Applications $application) {
        $options = Zend_Registry::get('options');

        //remove files
        $this->_deleteApplicationFiles($application->files);

        //remove application
        parent::delete($application);
    }

    public function getFilterStats() {
        $select = $this->select()
                    ->setIntegrityCheck(false)
                    ->from(array('a' => $this->_name), array('w.work_type_id','count(*) as size'))
                    ->join(array('w' => $this->getTableName('work_types')), 'a.work_type_id= w.work_type_id', null)
                    ->group('w.work_type_id');

        return $this->fetchAll($select);
    }

    public function getVoteStats() {
        $select = $this->select()
            ->setIntegrityCheck(false)
            ->from(array('a' => $this->_name), array('v.value','count(*) as size'))
            ->joinLeft(array('v' => $this->getTableName('votes')), 'a.application_id= v.application_id', null)
            ->group('v.value');

        return $this->fetchAll($select);
    }

    protected function _getUserDir($application, $oldData) {
        $id = $application->application_id;
        $options = Zend_Registry::get('options');

        //create folder for user's files
        $uploadDir = APPLICATION_PATH . '/../public' . $options['upload']['applications'] . '/';

        //create edition dir
        $edition = new Application_Model_Editions($application->edition_id);
        $editionName = $edition->edition_name;
        if(!is_dir($uploadDir . $editionName)) {
            mkdir($uploadDir . $editionName);
            chmod($uploadDir . $editionName, 0777);
        }

        $uploadDir = $uploadDir . $editionName;

        $type = new Application_Model_WorkTypes($application->work_type_id);

        $userDir = $application->user->getUserUrlName() . '_' . $type->work_type_name . '_' . $id;
        if(!is_dir($uploadDir . '/' . $userDir) && $oldData == null) {
            //create new user dir
            mkdir($uploadDir . '/' . $userDir);
            chmod($uploadDir . '/' . $userDir, 0777);
        }
        elseif($oldData != null) {
            //rename old dir if necessary
            $oldDir = $this->_getOldUserDir($oldData);

            if($userDir != $oldDir) {
                $this->_renameOldUserDir($oldDir, $userDir, $editionName);
            }
        }

        return $editionName . '/' . $userDir;
    }

    protected function _getOldUserDir($application) {
        $file_path = $application->files[0]->path;
        $strippedEdition = substr($file_path, strpos($file_path, '/') + 1);
        $dir = substr($strippedEdition, 0, strpos($strippedEdition, '/'));

        return $dir;
    }

    protected function _renameOldUserDir($oldDir, $newDir, $edition) {
        $options = Zend_Registry::get('options');
        $dirPath = APPLICATION_PATH . '/../public' . $options['upload']['applications'] . '/' . $edition . '/';

        rename($dirPath . $oldDir, $dirPath . $newDir);
    }

    protected function _saveUserFiles(Application_Model_Applications $application, $userDir, Application_Model_Applications $oldData = null) {
        $options = Zend_Registry::get('options');
        foreach($application->getFiles() as $file) {
            if (!$file->save) {
                continue;
            }

            /** @var Application_Model_Files $file */
            $source = APPLICATION_PATH . '/../public/assets/' . $file->path;
            $filePath = $userDir . '/' . basename($source);
            $destination = APPLICATION_PATH . '/../public' . $options['upload']['applications'] . '/' . $filePath;

            try {
                rename($source, $destination);
                $file->path = $filePath;
                $file->application_id = $application->application_id;
                $file->save();

                if ($oldData && $oldFile = $oldData->getFile($file->type)) {
                    if ($oldFile->getFileName() != $file->getFileName() && file_exists($oldFile->getAbsolutePath())) {
                        unlink($oldFile->getAbsolutePath());
                    }
                }
            }
            catch (Exception $e) {
                var_dump($e->getMessage());
            }
        }
    }

    protected function _deleteApplicationFiles($files) {
        $options = Zend_Registry::get('options');
        foreach($files as $file) {
            $file->delete();
            $dir = APPLICATION_PATH . '/../public' . $options['upload']['applications'] . '/' . substr($file->path, 0, strrpos($file->path, '/'));
            $path = APPLICATION_PATH . '/../public' . $options['upload']['applications'] . '/' . $file->path;
            if (file_exists($path) && !is_dir($path)) {
                unlink($path);
            }
        }

        rmdir($dir);
    }
}

