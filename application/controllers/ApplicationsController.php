<?php

class ApplicationsController extends STGU_Controller_Action {
    public function init() {
        parent::init();
        $this->view->page_title = 'application_title';
        $this->view->active_menu = 'application';
        $this->view->headScript()->appendFile($this->view->baseUrl('js/jquery-2.1.3.min.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/scripts.js'));
    }

    public function indexAction() {
        if (Zend_Registry::get('role') != 'admin' && Zend_Registry::get('user')->user_id == null) {
            $this->_redirectToRoute(array(), 'root');
        }
        $this->view->active_menu = 'application_list';
        $application = new Application_Model_Applications();
        $this->view->applications = $application->getApplications();
    }

    public function newAction() {
    }

    public function addAction() {
        $appSettings = Zend_Registry::get('appSettings');
        $form = $this->_getApplicationForm();
        $session = new Zend_Session_Namespace('applicationForm');
        if(isset($session->form)) {
            $values = $session->form;
            $form->populate($values);
        }

        $request = $this->getRequest();

        if($request->isPost()) {
            //form has been submited
            $data = $this->_prepareParams($request, $form);
            if ($form->isValid($data)) {
                $data = $this->_handleFiles($data,$form,'new');
                $user = $this->_saveUser($data);
                if ($form->getElement('newsletter') && $form->getElement('newsletter')->getValue() == 1) {
                    $this->_subscribe($user);
                }
                $this->_saveApplication($data, $user);
                $this->_redirectToRoute(array(), 'application_success');
            }
            else {
                $this->_handleFiles($data, $form, 'new');
            }
        }
        else {//no form has been submited
            $form->getElement('edition_id')->setValue($appSettings->edition->edition_id);
        }

        $this->view->form = $form;
    }

    /**
     * Edit application
     */
    public function editAction() {
        if ($id = $this->getRequest()->getParam('id', null)) {
            $application = new Application_Model_Applications($id);
            $this->_checkApplicationAccess($application);
            $form = new Application_Form_Application_Entry('new', 0);

            if ($this->getRequest()->isPost()) {
                $appData  = $this->getRequest()->getParams();
                $appData['entry']['application_id'] = $id;
                if ($form->isValid($appData)) {
                    $appData = $this->_handleFiles($appData,$form,'edit');
                    $appData['entry']['active'] = 1;
                    $appData['entry'] = array(1 => $appData['entry']);
                    $this->_saveApplication($appData);
                    $this->_redirectToRoute(array(), 'application_list');
                }
                else {

                }
            }
            else {
                $form->populate($application->prepareFormArray());
            }
            $form->setDecorators(array(
                array('ViewScript', array('viewScript' => 'forms/_applicationEditForm.phtml'))
            ));
            $this->view->form = $form;
        }
        else {
            $this->_redirectToRoute(array(), 'application_list');
        }
    }

    public function deleteAction() {
        if ($id = $this->getRequest()->getParam('id', null)) {
            $application = new Application_Model_Applications($id);
            $this->_checkApplicationAccess($application);
            $application->delete();
        }
        else {
            $this->_redirectToRoute(array(), 'application_list');
        }
    }

    /**
     * Show application data
     *
     * @throws Zend_Exception
     */
    public function showAction() {
        if ($id = $this->getRequest()->getParam('id', null)) {

            $this->_addFancybox();

            $application = new Application_Model_Applications($id);
            $this->_checkApplicationAccess($application);
            $this->view->active_menu = 'application_list';
            $this->view->application = $application;
        }
        else {
            $this->_redirectToRoute(array(), 'application_list');
        }
    }

    public function successAction() {

    }

    public function voteAction() {
        if (Zend_Registry::get('role') != 'admin' && Zend_Registry::get('user')->user_id == null) {
            $this->_redirectToRoute(array(), 'root');
        }
        $this->view->headScript()->appendFile($this->view->baseUrl('js/voting.js'));
        $this->view->active_menu = 'application_list';
        $application = new Application_Model_Applications();
        $size = $this->getRequest()->getParam('size', 50) != 'all' ? $this->getRequest()->getParam('size', 50) : null;
        $page = $this->getRequest()->getParam('page', 1);
        $this->view->applications = $application->getApplications('a.application_id', $size, $page);
        $this->view->appVotes = $this->_getVoteStats($this->view->applications);
        $types = new Application_Model_WorkTypes();
        $this->view->types = $types->fetchAll();
        $this->view->typesStats = $this->_getTypeStats();
        $this->view->page = $page;
        $this->view->size = $size;
        $this->view->lastPage = $page == $application->getMaxPage($size);
    }

    public function fetchAction() {
        if (Zend_Registry::get('role') != 'admin' && Zend_Registry::get('user')->user_id == null) {
            $this->_redirectToRoute(array(), 'root');
        }
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $appId = $this->getRequest()->getParam('app_id', false);
        if ($appId) {
            $app = new Application_Model_Applications($appId);
            $json = $this->_getVoteAppJson($app, false);
            $response['success'] = true;
            $response['app_id'] = (int)$appId;
            $response['data'] = $json;

            $this->getResponse()->setHeader('Content-type', 'application/json');
            $this->getResponse()->setBody(Zend_Json::encode($response));
            $this->getResponse()->sendResponse();
            exit;
        }
    }

    /**
     *
     * @param Application_Model_Applications $application
     * @throws Zend_Exception
     */
    protected function _checkApplicationAccess(Application_Model_Applications $application) {
        $user = Zend_Registry::get('user');
        if (Zend_Registry::get('role') != 'admin' && $application->user_id != $user->user_id) {
            $this->_redirectToRoute(array(), 'application_list');
        }
    }

    /**
     * Process files sent through application form
     *
     * @param array                        $data
     * @param Application_Form_Application $form
     * @param string                       $type
     * @return Application_Form_Application
     * @throws Zend_Exception
     */
    protected function _handleFiles($data, $form, $type = 'new') {
        $fileForms = array('mono_sign', 'color_sign', 'identification_1', 'identification_2', 'identification_3');
        $options = Zend_Registry::get('options');
        $loopArray = $type == 'new' ? $data['entry'] : array('entry' => array($data['entry']));

        foreach($loopArray as $id => $entryData) {
            foreach($fileForms as $field) {
                $fieldPrefix = $type == 'new' ? "entry[{$id}]" : 'entry';
                $fieldCache = $type == 'new' ? $field.'_'.$id : $field.'_0';
                $entryForm = $type == 'new' ? $form->getSubForm("{$fieldPrefix}") : $form;
                $sf = $entryForm->getSubForm("{$fieldPrefix}[{$field}]");
                $sf = $this->_cacheFile($options['upload']['cache'], $sf, $fieldCache);
                $cachedFile = ($sf->getElement($fieldCache . 'Cache')->getValue());
                if($cachedFile != null) {
                    if ($type == 'new') {
                        $data['entry'][$id][$field][$field . 'Cache'] = $cachedFile;
                        $data['entry'][$id][$field]['save'] = true;
                    }
                    else {
                        $data['entry'][$field][$field . 'Cache'] = $cachedFile;
                        $data['entry'][$field]['save'] = preg_match('/^cache\//', $cachedFile);
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Prepare application form
     *
     * @return Application_Form_Application
     */
    protected function _getApplicationForm() {
        $user = Zend_Registry::get('user');
        $applications_left = 5 - count($user->applications);
        $form = new Application_Form_Application('new', $applications_left);
        $form->setAction($this->view->url(array(), 'application_add'));
        $form->setDecorators(array(
            array('ViewScript', array('viewScript' => 'forms/_applicationForm.phtml'))
        ));

        if(!$this->view->user->isEmpty() && $this->view->user->role == 'user') {
            //use current user as applicant
            $form->removeSubForm('user');
            $form->setUser($this->view->user);
            $options = Zend_Registry::get('options');
            $api = new Mailchimp_Api($options['mailchimp']['apikey']);
            $result = $api->searchMembers($this->view->user->email);
            if (isset($result['exact_matches']) && $result['exact_matches']['total'] == 1) {
                $form->removeElement('newsletter');
            }
        }

        return $form;
    }

    /**
     * @param Zend_Controller_Request_Abstract $request
     * @param Application_Form_Application     $form
     * @return array
     */
    protected function _prepareParams(Zend_Controller_Request_Abstract $request, Application_Form_Application $form) {

        $params = $request->getParams();
        $entries = count($params['entry']);
        for($id= 1; $id <= 5; $id++) {
            if (!array_key_exists($id, $params['entry'])) {
                $form->removeSubForm('entry['.$id.']');
                continue;
            }
            $entryData = $params['entry'][$id];
            if($entryData['active'] != 1) {
                unset($params['entry'][$id]);
                $form->removeSubForm('entry['.$id.']');
            }
            else {
                if (!array_key_exists('mono_sign',$entryData) || !isset($entryData['mono_sign']["mono_sign_{$id}Cache"])) {
                    $form->getSubForm("entry[{$id}]")->getSubForm("entry[{$id}][mono_sign]")->getElement("mono_sign_{$id}")->setRequired(true);
                }
            }
        }

        return $params;
    }

    /**
     * Save application user
     *
     * @param array $data
     * @return Application_Model_Users
     */
    protected function _saveUser($data) {

        if (array_key_exists('user', $data)) {
            $user = new Application_Model_Users();
            $user->find(array('email' => $data['user']['email']));
            if ($user->user_id == null) {
                $user->populateFromForm($data['user']);
                $user->save();
            }
        }
        elseif (Zend_Registry::get('user')) {
            $user = Zend_Registry::get('user');
        }
        return $user;
    }

    /**
     *
     * @param array                   $data
     * @param Application_Model_Users $user
     * @return array
     */
    protected function _saveApplication($data, Application_Model_Users $user = null) {

        $errors = array('success' => true, 'messages' => array());
        foreach($data['entry'] as $id => $entryData) {

            try {
                $id = array_key_exists('application_id', $entryData) ? $entryData['application_id'] : null;
                $application = new Application_Model_Applications($id);
                $application->populateFromForm($entryData);
                if ($user) {
                    $application->user = $user;
                }
                if (array_key_exists('rules_agreement', $data)) {
                    $application->rules_agreement = $data['rules_agreement'];
                }
                if (array_key_exists('copyright', $data)) {
                    $application->copyright = $data['copyright'];
                }

                $application->save();
            }
            catch (Exception $e) {
                $errors['success'] = false;
                $errors['messages'][$id] = $e->getMessage();
            }
        }

        return $errors;
    }

    /**
     * Add fancybox related files
     */
    protected function _addFancybox() {
        $this->view->headLink()->appendStylesheet($this->view->baseUrl('js/fancybox/jquery.fancybox-1.3.4.css'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/fancybox/jquery.fancybox-1.3.4.js'));
        $this->view->headScript()->appendFile($this->view->baseUrl('js/fancybox/jquery.easing-1.3.pack.js'));
    }

    /**
     * Subscribe user to mailchimp newsletter
     *
     * @param Application_Model_Users $user
     * @throws Zend_Exception
     */
    protected function _subscribe(Application_Model_Users $user) {
        $options = Zend_Registry::get('options');
        $api = new Mailchimp_Api($options['mailchimp']['apikey']);
        $merge_vars = array(
            'FNAME' => $user->getFirstname(),
            'LNAME' => $user->getLastname()
        );
        $api->listSubscribe($options['mailchimp']['list-id'], $user->email, $merge_vars);
    }

    /**
     * Convert applications array into table
     *
     * @param array $applications
     * @return array
     */
    protected function _getVoteAppJson($applications, $returnCollection = true) {
        $json = array();
        if (!is_array($applications)) {
            $applications = array($applications);
        }
        foreach($applications as $application) {
            $convert = $application->application_id > 2796 ? true: false;
            /** @var Application_Model_Applications $application */
            $files = array(
                'mono_sign' => $application->getFile('mono_sign') ? $application->getFile('mono_sign')->getImageUrl('small', $convert) : null,
                'color_sign' => $application->getFile('color_sign') ? $application->getFile('color_sign')->getImageUrl('small', $convert) : null,
                'identification_1' => $application->getFile('identification_1') ? $application->getFile('identification_1')->getImageUrl('big') : null,
                'identification_2' => $application->getFile('identification_2') ? $application->getFile('identification_2')->getImageUrl('big') : null,
                'identification_3' => $application->getFile('identification_3') ? $application->getFile('identification_3')->getImageUrl('big') : null,
            );

            foreach($files as $type => $url) {
                if ($url != null) {
                    $files[$type] = $this->view->baseUrl('assets/applications/' . $url);
                }
            }

            /** @var Application_Model_Applications $application */
            $json[$application->application_id] = array(
                'title' => $application->work_subject,
                'description' => $application->work_desc,
                'category' => $application->work_type->work_type_name,
                'client' => $application->customer,
                'year' => $application->project_year,
                'files' => $files,
                'projectLink' => $this->view->url(array('id' => $application->application_id), 'application_view'),
                'vote' => $application->getVote()->value
                );
        }
        return $returnCollection ? $json : array_shift($json);
    }

    protected function _getVoteStats($applications) {
        $application = new Application_Model_Applications();
        $statistics = $application->getVoteStats();

        return $statistics;
    }

    protected function _getTypeStats() {

        $application = new Application_Model_Applications();
        $statistics = $application->getFiltersStats();

        return $statistics;
    }
}
