<?php

class UsersController extends STGU_Controller_Action {

    public function init() {
        parent::init();
    }

    public function editAction() {
        $id = $this->getRequest()->getParam('id', null);
        /** @var Application_Model_Users $user */
        $user = Zend_Registry::get('user');
        if ($id && $id == $user->user_id) {
            $form = new Application_Form_User('form', null, Zend_Registry::get('role'));

            if ($this->getRequest()->isPost()) {
                if ($form->isValid($this->getRequest()->getPost())) {
                    $profile = new Application_Model_Users();
                    $profile->populateFromForm($form->getValues());
                    $profile->setUserPassword($form->password->getValue());
                    $profile->save();
                    $this->_redirectToRoute(array(), 'application_list');
                }
            }
            else {
                $form->populate($user->prepareFormArray());
            }
            $form->setDecorators(array(
                array('ViewScript', array('viewScript' => 'forms/_userForm.phtml'))
            ));
            $this->view->form = $form;
        }
        else {
            $this->_redirectToRoute(array(), 'root');
        }
    }
}

