<?php

class VotesController extends STGU_Controller_Action {

    public function saveAction() {

        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        /** @var Zend_Controller_Request_Http $request */
        $request = $this->getRequest();
        if ($request->isPost() && $request->getPost('app_id', false)) {
            $vote = new Application_Model_Votes();
            $vote->findByApplication($request->getPost('app_id'));
            if ($vote->vote_id && $request->getPost('value') === '') {
                $vote->delete();
            }
            else {
                $vote->application_id = $request->getPost('app_id');
                $vote->value = $request->getPost('value', null);
                $vote->save();
            }
            echo Zend_Json::encode(array(
                'success' => true,
                'app_id' => $vote->application_id,
                'value' => $request->getPost('value')
            ));
        }
        else {
            $this->_redirectToRoute(array(), 'application_vote');
        }
    }

}
