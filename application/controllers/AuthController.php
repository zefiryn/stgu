<?php
/**
 * @package AuthController
 */

/**
 * Authentication controller
 *
 * @author zefiryn
 * @since  Feb 2011
 */
class AuthController extends Zefir_Controller_Action {

    /**
     * Inital
     *
     * @access public
     * @return void
     */
    public function init() {
        parent::init();
    }

    public function indexAction() {
        $this->loginAction();
        $this->_helper->viewRenderer('login');
    }

    /**
     * Handle login
     *
     * @access public
     * @return void
     */
    public function loginAction() {

        $this->_checkLoggedIn();

        $request = $this->getRequest();
        $loginForm = new Application_Form_Login();
        $loginForm->setDecorators(array(
            array('ViewScript', array('viewScript' => 'forms/_loginForm.phtml'))
        ));

        if($request->isPost()) {
            $user = new Application_Model_Users();
            $user->getUser($request->getPost('email'));
            if($user->role != 'user' && $loginForm->isValid($request->getPost())) {

                $result = $this->_authenticateUser($loginForm->getValue('email'), $loginForm->getValue('password'));
                if ($result === false) {
                    $this->view->wrongLogin = true;
                }
            }
            else if ($user->role == 'user') {
                $this->flashMe('Logowanie zostało zablokowane', 'ERROR');
            }
        }
        $this->view->loginForm = $loginForm;
        $this->view->page_title = 'login_title';
    }

    /**
     * Handle logout
     *
     * @access public
     * @return void
     */
    public function logoutAction() {
        Zend_Auth::getInstance()->clearIdentity();
        $templateSession = new Zend_Session_Namespace('template');
        $templateSession->template_name = null;
        setcookie("template_name", null, time() - 60 * 60, '/');
        $this->flashMe('logout_success', 'SUCCESS');
        $this->_redirectToRoute(array(), 'root');
    }

    public function rememberAction() {


        $form = new Application_Form_Restore();
        $form->setDecorators(array(
            array('ViewScript', array('viewScript' => 'forms/_restoreForm.phtml'))
        ));

        if($this->getRequest()->isPost()) {
            $user = new Application_Model_Users();
            $user->getBy('email', $this->getRequest()->getParam('email'));

            if($user->user_id) {
                $user->token = bin2hex(mcrypt_create_iv(32, MCRYPT_DEV_RANDOM));
                $user->token_time = time();
                $user->save();
                $this->_sendResetMail($user);
                $this->renderScript('auth/mail_send.phtml');
            }
            else {
                $this->view->wrongLogin = true;
            }
        }
        $this->view->form = $form;
    }

    public function resetAction() {
        if ($hash = $this->getRequest()->getParam('hash', false)) {

            $user = new Application_Model_Users();
            $user->getBy('token', $hash);
            if($user->user_id && $user->isTokenValid()) {
                if ($this->getRequest()->isPost()) {
                    $newPass = $this->getRequest()->getParam('password');
                    $user->setUserPassword($newPass);
                    $user->token = null;
                    $user->token_time = null;
                    $user->save();
                    $this->_authenticateUser($user->email, $newPass);
                }
                $loginForm = new Application_Form_Reset();
                $loginForm->setAction($this->view->url(array('hash' => $hash), 'auth_reset'));
                $loginForm->setDecorators(array(
                    array('ViewScript', array('viewScript' => 'forms/_resetForm.phtml'))
                ));
                $this->view->form = $loginForm;
            }
            else {
                $this->_redirectToRoute(array(), 'root');
            }
        }
        else {
            $this->_redirectToRoute(array(), 'root');
        }
    }

    protected function _checkLoggedIn() {
        /** @var Application_Model_Users $user */
        $user = Zend_Registry::get('user');
        if($user->user_id != null) {
            $this->_redirectToRoute(array(), 'root');
        }
    }

    protected function _sendResetMail(Application_Model_Users $user) {
        $options = Zend_Registry::get('options');
        $mail = new Zend_Mail('UTF8');

        $mail->setFrom($options['mail']['from'], $this->view->translate('reset_email_from'));
        $mail->addTo($user->email, $user->getUserFullName());
        $mail->setSubject($this->view->translate('reset_email_subject'));
        $body = $this->view->translate('reset_email_body');
        $body = sprintf($body, $user->name, $this->view->url(array('hash' => $user->token), 'auth_reset'));
        $mail->setBodyText($body);
        $mail->send();
    }

    protected function _authenticateUser($email, $password) {
        $options = Zend_Registry::get('options');
        $adapter = new Zend_Auth_Adapter_DbTable(
            Zend_Db_Table::getDefaultAdapter(),
            $options['resources']['db']['params']['prefix'] . 'users',
            'email',
            'password',
            'SHA1(CONCAT(?,salt))');

        $adapter->setIdentity($email);
        $adapter->setCredential($password);

        $authentication = Zend_Auth::getInstance();
        $result = $authentication->authenticate($adapter);

        if($result->isValid()) {
            //reset the template settings
            $templateSession = new Zend_Session_Namespace('template');
            $templateSession->template_name = null;

            //redirect to the page from
            $authNamespace = Zend_Session::namespaceGet('auth');

            $this->flashMe('login_success', 'SUCCESS');

            if(isset($authNamespace['redirect']) && $authNamespace ['redirect'] != null) {
                $newAuth = new Zend_Session_Namespace('auth');
                $newAuth->redirect = null;
                $this->redirect($authNamespace['redirect']);
            }
            else {
                $this->_redirectToRoute(array(), 'root');
            }
        }
        else {
            return false;
        }
    }
}