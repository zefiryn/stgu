<?php

class ManageController extends STGU_Controller_Action {

    public function init() {
        $this->_helper->layout()->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);

        return parent::init();
    }

    public function infoAction() {
        phpinfo();
    }

    public function resizeAction() {
        ini_set('max_execution_time', 0);
        $files = new Application_Model_Files();
        $force = $this->getRequest()->getParam('force', false);
        $type = $this->getRequest()->getParam('type', false);
        foreach($files->getAll() as $file) {
            /** @var Application_Model_Files $file */
            if($type && $type != $file->getFileType()) {
                continue;
            }
            if($file->getFileType() == 'pdf') {
                $file->convertToImage();
                $file->path = $file->path . '.png';
            }
            /** @var Application_Model_Files $file */
            foreach($file->getThumbnails() as $key) {
                try {
                    if($force) {
                        $file->getDbTable()->rerunResize($file, 'path', APPLICATION_PATH . '/../public/assets/applications/', $key);
                    }
                    else {
                        $file->getImageUrl($key);
                    }
                }
                catch (Exception $e) {
                    $this->_log($e->getMessage());
                    echo $e->getMessage() . '<br />';
                }
            }
        }
    }

    public function convertAction() {
        $file = new Application_Model_Files(357);
        $im = new Imagick();
        $im->setResolution(200, 200);
        $im->readimage($file->getAbsolutePath());
        $im->setImageFormat('png');
        $im->setImageColorspace(Imagick::COLORSPACE_CMYK);
        $im->transformImageColorspace(Imagick::COLORSPACE_SRGB);
        $im->writeImage($file->getAbsolutePath() . '.png');
    }

}

