<?php

class IndexController extends STGU_Controller_Action {

    public function init() {
        parent::init();
    }

    public function indexAction() {
        $this->_helper->layout->setLayout('working');
    }

    public function mainAction() {

    }

    public function aboutAction() {
        $this->view->page_title = 'about_title';
        $this->view->active_menu = 'about';
    }

    public function rulesAction() {
        $this->view->page_title = 'rules_title';
        $this->view->active_menu = 'rules';
    }

    public function contactAction() {
        $this->view->page_title = 'contact_title';
        $this->view->active_menu = 'contact';
    }
}

