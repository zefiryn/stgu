<?php
/**
 * Bootstrap file
 *
 * @package Bootstrap
 */

/**
 * Bootstrap class
 */
class Bootstrap extends Zend_Application_Bootstrap_Bootstrap {
    /**
     * Main init
     *
     * @access private
     * @return void
     */
    protected function _init() {
        //Zend_Session::start();
        iconv_set_encoding('internal_encoding', 'UTF-8');
    }

    /**
     * Init configuration
     *
     * @access private
     * @return void
     */
    protected function _initConfig() {
        $db_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/db.ini');
        $this->setOptions($db_config->toArray());
        Zend_Registry::set('options', $this->getOptions());

        //set the db
        $config = Zend_Registry::get('options');
        $db = Zend_Db::factory($config['resources']['db']['adapter'], $config['resources']['db']['params']);
        Zend_Db_Table::setDefaultAdapter($db);

        //set redirect absolute path if necessary
        if($config['redirect']['absolute'] === true) {
            $helper = new Zefir_Controller_Action_Helper_Homepl();
            $hb = Zend_Controller_Action_HelperBroker::addHelper($helper);
        }
    }

    /**
     * Init routing routines
     *
     * @access private
     * @return void
     */
    protected function _initRouting() {
        $frontController = $this->bootstrap('frontController');
        $router = $this->getResource('frontController')->getRouter();

        $route = new Zend_Controller_Router_Route(
            'login',
            array('controller' => 'auth',
                'action' => 'login'));
        $router->addRoute('login', $route);

        $route = new Zend_Controller_Router_Route(
            'logout',
            array(	'controller' => 'auth',
                'action' => 'logout'));
        $router->addRoute('logout', $route);

        /**
         * add routing from the ini file
         */
        $route_config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/route.ini', 'production');
        $router->addConfig($route_config, 'routes');


    }

    /**
     * Init access controll list
     *
     * @access private
     * @return Zend_Acl $acl
     */
    protected function _initAcl() {
        require_once 'Zend/Acl/Role.php';
        require_once 'Zend/Acl/Resource.php';

        $acl = new Zend_Acl();
        $guestRole = new Zend_Acl_Role(new Zend_Acl_Role('guest'));

        $acl->addRole($guestRole)
            ->addRole(new Zend_Acl_Role('user'), $guestRole)
            ->addRole(new Zend_Acl_Role('juror'), 'user')
            ->addRole(new Zend_Acl_Role('admin'), 'user');

        //resources
        $acl->addResource(new Zend_Acl_Resource('error'));
        $acl->addResource(new Zend_Acl_Resource('index'));
        $acl->addResource(new Zend_Acl_Resource('auth'));
        $acl->addResource(new Zend_Acl_Resource('applications'));
        $acl->addResource(new Zend_Acl_Resource('users'));
        $acl->addResource(new Zend_Acl_Resource('manage'));
        $acl->addResource(new Zend_Acl_Resource('votes'));

        //clearance
        $acl->allow(null, array('error', 'index'), null);
        $acl->allow(null, array('auth'), array('index', 'login', 'remember', 'reset'));
        $acl->allow(null, array('users'), array('show', 'edit', 'restore', 'delete'));
        $acl->allow(null, array('applications'), array('new', 'success', 'index', 'show', 'edit'));
        $acl->allow('user', array('auth'), array('logout'));

        $acl->allow('admin', array('users'), array('new', 'index'));
        $acl->allow('admin', null, null);
        $acl->allow('admin', 'applications', null);

        $acl->allow('juror', 'applications', array('vote', 'fetch'));
        $acl->allow('juror', 'votes', array('save'));
        $acl->deny('juror', array('applications'), array('new', 'success', 'index', 'show', 'edit'));

        Zend_Registry::set('acl', $acl);

        return $acl;
    }

    /**
     * Init action helpers
     *
     * @access private
     * @return void
     */
    protected function _initActionHelpers() {
        Zend_Controller_Action_HelperBroker::addHelper(
            new Zefir_Action_Helper_UserSession()
        );
        Zend_Controller_Action_HelperBroker::addHelper(
            new Zefir_Action_Helper_Flash()
        );
        Zend_Controller_Action_HelperBroker::addHelper(
            new Zefir_Action_Helper_Localization()
        );
        Zend_Controller_Action_HelperBroker::addHelper(
            new STGU_Action_Helper_AppSettings()
        );
    }

    protected function _initMail() {

        $options = Zend_Registry::get('options');

        if(array_key_exists('mail', $options)) {
            $transport = new Zend_Mail_Transport_Smtp($options['mail']['host'], $options['mail']['smtp']);
            Zend_Mail::setDefaultTransport($transport);
        }
    }

    /**
     * Init View class
     *
     * @access private
     * @return Zend_View an instance of Zend_View class or similar
     */
    protected function _initView() {

        $view = new Zefir_View_Template();
        $view->addHelperPath('Zefir/View/Helper', 'Zefir_View_Helper');

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $viewRenderer->setView($view)
            ->setViewScriptPathSpec(':controller/:action.:suffix')
            ->setViewScriptPathNoControllerSpec(':action.:suffix')
            ->setViewSuffix('phtml');

        //initialize layout
        $layout = Zend_Layout::getMvcInstance();
        $layout->setViewSuffix('phtml');

        return $view;

    }

    /**
     * Init placeholders for the view
     *
     * @access private
     * @return void
     */
    protected function _initPlaceholders() {

        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->assign('doctype', 'XHTML1_STRICT');

        // Set the initial title
        $view->headTitle()->setSeparator(' :: ');

        //set the js files
        $options = Zend_Registry::get('options');

    }

}

