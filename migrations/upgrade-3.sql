SET foreign_key_checks = 0;
TRUNCATE work_types;
ALTER TABLE work_types MODIFY COLUMN work_type_name VARCHAR(50);
INSERT INTO work_types VALUES (null,'Projekt studencki'),(null, 'Projekt zrealizowany'),(null, 'Projekt niezrealizowany');
SET foreign_key_checks = 1;