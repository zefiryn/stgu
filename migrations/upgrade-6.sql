
CREATE TABLE IF NOT EXISTS `votes` (
  `vote_id` SMALLINT NOT NULL AUTO_INCREMENT,
  `application_id` SMALLINT NOT NULL,
  `value` TINYINT NOT NULL,
  PRIMARY KEY (`vote_id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;