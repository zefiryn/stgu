SET foreign_key_checks = 0;
ALTER TABLE users ADD COLUMN salt VARCHAR(15) after password;
UPDATE users SET salt = substring_index(password, ':',-1);
UPDATE users SET password = substring_index(password, ':',1);
SET foreign_key_checks = 1;

