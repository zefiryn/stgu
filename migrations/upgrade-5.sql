SET foreign_key_checks = 0;
ALTER TABLE users ADD COLUMN token VARCHAR(200);
ALTER TABLE users ADD COLUMN token_time INT(11);
SET foreign_key_checks = 1;