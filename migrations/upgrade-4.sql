SET foreign_key_checks = 0;
ALTER TABLE files ADD COLUMN type VARCHAR(50);
SET foreign_key_checks = 1;