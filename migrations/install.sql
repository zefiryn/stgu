CREATE TABLE partners (
  partner_id smallint(6) NOT NULL AUTO_INCREMENT,
  partner_name varchar(100) NOT NULL,
  partner_link varchar (400),
  partner_file varchar(100) NOT NULL,
  PRIMARY KEY (partner_id)  
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `applications` (
  `application_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `edition_id` smallint(6) DEFAULT NULL,
  `user_id` int(6) DEFAULT NULL,
  `work_subject` varchar(300) NOT NULL,
  `work_type_id` smallint(6) DEFAULT NULL,
  `work_desc` text NOT NULL,
  `work_short_desc` text NOT NULL,
  `studio_name` varchar(800) NOT NULL,
  `customer` varchar(500) NOT NULL,
  `project_webpage` varchar(300) NOT NULL,
  `project_year` int(6) NOT NULL,
  `application_date` int(11) NOT NULL,
  `miniature` varchar(35) DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`application_id`),
  KEY `edition_id` (`edition_id`),
  KEY `user_id` (`user_id`),
  KEY `work_type_id` (`work_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `captions` (
  `caption_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  PRIMARY KEY (`caption_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `diplomas` (
  `diploma_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `edition_id` smallint(6) DEFAULT NULL,
  `name` char(150) NOT NULL,
  `surname` char(200) NOT NULL,
  `slug` varchar(200) NOT NULL,
  `email` varchar(35) NOT NULL,
  `work_type_id` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`diploma_id`),
  KEY `edition_id` (`edition_id`),
  KEY `work_type_id` (`work_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `diploma_fields` (
  `diploma_field_id` int(11) NOT NULL AUTO_INCREMENT,
  `diploma_id` smallint(6) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `field_id` smallint(6) NOT NULL,
  `entry` text NOT NULL,
  PRIMARY KEY (`diploma_field_id`),
  KEY `diploma_id` (`diploma_id`),
  KEY `lang_id` (`lang_id`),
  KEY `field_id` (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `diploma_files` (
  `file_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `diploma_id` smallint(6) NOT NULL,
  `path` varchar(150) NOT NULL,
  `file_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `diploma_id` (`diploma_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `disputes` (
  `dispute_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `application_id` smallint(6) NOT NULL,
  PRIMARY KEY (`dispute_id`),
  KEY `user_id` (`user_id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `editions` (
  `edition_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `edition_name` char(10) NOT NULL,
  `publish_results` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`edition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `fields` (
  `field_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `field_name` varchar(100) NOT NULL,
  PRIMARY KEY (`field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ;

INSERT INTO `fields` (`field_id`, `field_name`) VALUES
(1, 'work_subject'),
(2, 'work_short_desc'),
(3, 'work_desc'),
(4, 'studio_name'),
(5, 'customer'),
(6, 'project_webpage'),
(7, 'project_year');

CREATE TABLE IF NOT EXISTS `files` (
  `file_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `application_id` smallint(6) NOT NULL,
  `path` varchar(150) NOT NULL,
  `file_desc` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`file_id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `jurors` (
  `juror_id` int(11) NOT NULL AUTO_INCREMENT,
  `juror_name` varchar(255) NOT NULL,
  `wage` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`juror_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `languages` (
  `lang_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `lang_code` char(4) NOT NULL,
  PRIMARY KEY (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

INSERT INTO `languages` (`lang_id`, `name`, `lang_code`) VALUES
(1, 'Polski', 'pl'),
(2, 'English', 'en');

CREATE TABLE pages (
  page_id smallint(6) NOT NULL AUTO_INCREMENT,
  page_name varchar(100) NOT NULL,
  lang_id int NOT NULL,
  header varchar(300) NOT NULL,
  content TEXT NOT NULL,
  PRIMARY KEY (page_id),
  FOREIGN KEY(lang_id)
    REFERENCES languages(lang_id)
    ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=INNODB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `localizations` (
  `item_id` int(11) NOT NULL AUTO_INCREMENT,
  `caption_id` int(11) NOT NULL,
  `lang_id` int(11) NOT NULL,
  `text` text NOT NULL,
  PRIMARY KEY (`item_id`),
  KEY `lang_id` (`lang_id`),
  KEY `caption_id` (`caption_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `settings` (
  `current_edition` smallint(6) NOT NULL DEFAULT '0',
  `template_default` smallint(6) DEFAULT NULL,
  `max_file_size` int(11) NOT NULL,
  `date_format` varchar(10) NOT NULL,
  `max_files` smallint(6) NOT NULL,
  `work_start_date` int(11) NOT NULL,
  `work_end_date` int(11) NOT NULL,
  `application_deadline` int(11) NOT NULL,
  `result_date` int(11) NOT NULL,
  PRIMARY KEY (`current_edition`),
  KEY `template_default` (`template_default`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `stages` (
  `stage_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `stage_name` char(10) NOT NULL,
  `stage_max_vote` smallint(6) NOT NULL,
  `qualification_score` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `active` tinyint(4) DEFAULT '1',
  `translate` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`stage_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `template_settings` (
  `template_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `template_name` varchar(20) NOT NULL,
  `news_limit` int(11) NOT NULL,
  PRIMARY KEY (`template_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;


INSERT INTO `template_settings` (`template_id`, `template_name`, `news_limit`) VALUES
(1, 'stgu', 5);

CREATE TABLE IF NOT EXISTS `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `nick` varchar(50) NOT NULL,
  `password` char(64) NOT NULL,
  `name` char(150) NOT NULL,
  `surname` char(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `phone` varchar(15) NOT NULL,
  `email` varchar(35) NOT NULL,
  `show_email` tinyint(1) DEFAULT '0',
  `role` varchar(20) NOT NULL DEFAULT 'user',
  `juror_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

CREATE TABLE IF NOT EXISTS `votes` (
  `vote_id` int(11) NOT NULL AUTO_INCREMENT,
  `stage_id` smallint(6) NOT NULL,
  `juror_id` int(11) NOT NULL,
  `application_id` smallint(6) NOT NULL,
  `vote` int(11) NOT NULL,
  PRIMARY KEY (`vote_id`),
  KEY `stage_id` (`stage_id`),
  KEY `juror_id` (`juror_id`),
  KEY `application_id` (`application_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `work_types` (
  `work_type_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `work_type_name` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`work_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

INSERT INTO `stages` (`stage_id`, `stage_name`, `stage_max_vote`, `qualification_score`, `order`, `active`, `translate`) VALUES
(1, 'Stage 1', 1, 0, 1, 1, 0);

CREATE TABLE IF NOT EXISTS `board` (
  `person_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `edition_id` smallint(6) DEFAULT NULL,
  `name` varchar(100) NOT NULL,
  `image` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`person_id`),
  KEY `edition_id` (`edition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `board_descriptions` (
  `board_desc_id` smallint(6) NOT NULL AUTO_INCREMENT,
  `board_id` smallint(6) NOT NULL,
  `description` text NOT NULL,
  `lang_id` int(11) NOT NULL,
  PRIMARY KEY (`board_desc_id`),
  KEY `board_id` (`board_id`),
  KEY `lang_id` (`lang_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

ALTER TABLE `applications`
  ADD CONSTRAINT `applications_ibfk_1` FOREIGN KEY (`edition_id`) REFERENCES `editions` (`edition_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `applications_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `applications_ibfk_5` FOREIGN KEY (`work_type_id`) REFERENCES `work_types` (`work_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `diplomas`
  ADD CONSTRAINT `diplomas_ibfk_1` FOREIGN KEY (`edition_id`) REFERENCES `editions` (`edition_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `diplomas_ibfk_3` FOREIGN KEY (`work_type_id`) REFERENCES `work_types` (`work_type_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `diploma_fields`
  ADD CONSTRAINT `diploma_fields_ibfk_1` FOREIGN KEY (`diploma_id`) REFERENCES `diplomas` (`diploma_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `diploma_fields_ibfk_2` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`lang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `diploma_fields_ibfk_3` FOREIGN KEY (`field_id`) REFERENCES `fields` (`field_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `diploma_files`
  ADD CONSTRAINT `diploma_files_ibfk_1` FOREIGN KEY (`diploma_id`) REFERENCES `diplomas` (`diploma_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `disputes`
  ADD CONSTRAINT `disputes_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `disputes_ibfk_2` FOREIGN KEY (`application_id`) REFERENCES `applications` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `files`
  ADD CONSTRAINT `files_ibfk_1` FOREIGN KEY (`application_id`) REFERENCES `applications` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `localizations`
  ADD CONSTRAINT `localizations_ibfk_1` FOREIGN KEY (`lang_id`) REFERENCES `languages` (`lang_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `localizations_ibfk_2` FOREIGN KEY (`caption_id`) REFERENCES `captions` (`caption_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `settings`
  ADD CONSTRAINT `settings_ibfk_1` FOREIGN KEY (`current_edition`) REFERENCES `editions` (`edition_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `settings_ibfk_2` FOREIGN KEY (`template_default`) REFERENCES `template_settings` (`template_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `votes`
  ADD CONSTRAINT `votes_ibfk_1` FOREIGN KEY (`stage_id`) REFERENCES `stages` (`stage_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_ibfk_2` FOREIGN KEY (`juror_id`) REFERENCES `jurors` (`juror_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_ibfk_3` FOREIGN KEY (`application_id`) REFERENCES `applications` (`application_id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `board`
  ADD CONSTRAINT `board_ibfk_1` FOREIGN KEY (`edition_id`) REFERENCES `editions` (`edition_id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
ALTER TABLE  `board_descriptions` 
  ADD FOREIGN KEY (  `board_id` ) REFERENCES  `board` (`person_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD FOREIGN KEY (  `lang_id` ) REFERENCES  `languages` (`lang_id`) ON DELETE CASCADE ON UPDATE CASCADE ;